# chilli

### 介绍
chilli是一套数据应用开发及数据科学管理工具套件集,以阿里DataWorks/Dataphins/DataV、袋鼠云数据中台为蓝本，基于大数据生态中的开源组件，构建包括数据集成、数据存储、流批一体数据处理、数据仓库、数据治理、数据挖掘、任务调度、机器学习、数据可视化等数据开发任务的端到端、高集成、可扩展、一站式、沉侵式的数据中台解决方案。我们致力于通过提供智能应用程序、数据分析和咨询服务来提供最优解决方案。

#### Why
为什么做这个项目？
- 产品沉淀，将多个中小企业生产环境上线的大数据平台实践经验，以产品化的思维重新沉淀。
- 知识重组，将大数据技术、架构思路、编程经验、产品设计能力重新组装。
- 持续成长，借由改项目聚焦志同道合的朋友一起学习成长。


#### What

- 大数据基础平台，支持HDP、CDH及Apache原生大数据平台组件;
- 基于[CDAP](https://github.com/cdapio/cdap)开发大数据应用开发框架
- 基于[Davinci](https://github.com/edp963/davinci)开发BI工具及数据大屏
- 基于[DolphinScheduler](https://github.com/apache/incubator-dolphinscheduler)构建大数据调度平台。
- 选择型集成Webbank数据中台开源组件[linkis](https://github.com/WeBankFinTech/Linkis)及[DataSphereStudio](https://github.com/WeBankFinTech/DataSphereStudio)

##### 选型分析

###### 数据采集选型分析

- DataX [DataX](https://github.com/alibaba/DataX)、[DataX-Web](https://github.com/WeiYe-Jing/datax-web)
- FlinkX [FlinkX](https://github.com/DTStack/flinkx)、[FlinkX-Web](https://github.com/wxgzgl/flinkx-web)
- Streamsets [datacollector](https://github.com/streamsets/datacollector)
- NiFi [Nifi](https://github.com/apache/nifi)

###### CDAP分析

//TODO

###### BI选型分析

开源BI选择：为何不是metabase、metronplay、superset、redash？

###### 大屏选型分析

###### 调度选型分析

Oozie、Azakaba、Airflow


#### How
- 优先复用开源组件，如大数据基础平台选择基于HDP二次开发。
- 以最终用户为中心，重构前端界面，将开源组件中最终用户（运营、开发人员）需要重度使用的前端界面重写，提高一致性体验及沉侵式体验。
- 自上而下设计产品，先集中里面构建上层用户关系的功能，如BI工具、大屏设计器、报表设计器
- 以Java、Vue等国内主流技术为技术选型标准

#### When
从今开始守望,至死方休。

### 技术栈
#### 存储

- 分布式存储：HDFS、Minio
- 行式关系存储：MySQL、Oracle、PostgresSQL
- 列式存储：ClickHouse、Doris
- 列族存储：HBase、Cassandra
- 文档库：ElasticSearch、MongoDB
- 消息管道：Kafka
- 数据湖：Hudi

#### 计算

- 计算引擎：Presto、Hive、Spark
- 流处理：Spark Structure Streaming、Flink

#### 集成：

- DataX、FlinkX
- Filebeat
- Logstash

#### 前端技术栈

- Vue、React
- Element UI

#### 后端技术栈

- Spring Boot
- Spring Cloud
- MyBatis

### 软件功能

//TODO

### 软件架构
软件架构说明


### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
