# chilli-report

### 介绍
chilli-report定位于解决传统的报表相关工作，如报表制作、报表打印、报表导入导出等。

### 需求分析
报表工具在传统企业使用的比较广泛，主要用于财务、供应链、仓库等业务流程。尽管如此，随着信息化、无纸化的深入实施，报表工具的价值会越来越小。与此同时，经过信息化多年的发展，商业化的报表工具已经非常成熟。因此，现阶段开始做，报表工具建议只聚焦核心功能点，并依赖开源或者第三方免费工具集成。

本开源项目拟实现功能，以积木报表功能点为基础增强：
![功能](../pic/chilli-report-function.png)

### 竞品分析
#### 商业产品
##### 水晶报表
##### 金蝶报表设计器
##### 润乾报表设计器
#### 开源产品
##### UReport
- [urport源码](https://gitee.com/youseries/ureport/)
- [urport使用文档](http://wiki.bsdn.org/display/UR/UREPORT+2+Home)
##### 积木报表
- [积木报表源码](https://github.com/zhangdaiscott/jeecg-boot)
- [开源计划](http://jimureport.com/plan)
- [使用文档](http://report.jeecg.com/1835708)

### 功能设计


### 技术栈
#### 前端技术栈

- Vue、React
- Element UI

#### 后端技术栈

- Spring Boot
- Spring Cloud
- MyBatis

### 软件功能

//TODO

### 软件架构
![design](../pic/chilli-report-design.png)


### 安装教程

1.  xxxx
2.  xxxx
3.  xxxx

### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
